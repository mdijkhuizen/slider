/**
 * Circle slider component.
 *
 * @param {object} element Reference to element which will be the component.
 * @param {object} options Options for customizing component.
 */
function Slider(element, options)
{
	// Element is required value
	if (!element) {
		return false;
	}

	this.onMove = null;

	// Element references
	this.component = element;
	this.overlayLeft = null;
	this.overlayRight = null;
	this.mapper = null;
	this.button = null;

	// Properties
	this.angle = 0;
	this.sliding = false;
	this.steps = [];
	this.stepAngle = 1;

	this.center = {x: 0, y: 0};
	this.offset = {x: 0, y: 0};
	this.transformProperty = false;
	this.borderRadiusProperty = false;

	// Default values
	this.background = null;
	this.color = "";
	this.radius = 100;
	this.min = 0;
	this.max = 100;
	this.snap = false;
	this.step = 1;
	this.showSteps = true;
	this.borderSize = 30;

	// Options
	var testDiv = document.createElement("div");
	this.getBorderRadiusProperty(testDiv);
	this.getTransformProperty(testDiv);
	for (var key in options) {
		var value = options[key];
		switch(key) {
			case 'background':
				testDiv.style.borderColor = "";
				testDiv.style.borderColor = value;
				if (testDiv.style.borderColor.length > 0) {
					this.background = value;
				}
				break;
			case 'color':
				testDiv.style.borderColor = "";
				testDiv.style.borderColor = value;
				if (testDiv.style.borderColor.length > 0) {
					this.color = value;
				}
				break;
			case 'radius':
				if (!isNaN(value)) {
					this.radius = value;
				}
				break;
			case 'min':
				if (!isNaN(value)) {
					this.min = value;
				}
				break;
			case 'max':
				if (!isNaN(value)) {
					this.max = value;
				}
				break;
			case 'step':
				if (!isNaN(value)) {
					this.step = value;
				}
				if (this.step <= 0) {
					this.step = 1;
					this.showSteps = false;
				}
				break;
			case 'width':
				if (!isNaN(value)) {
					this.borderSize = value;
				}
				break;
			case 'solid':
				if (value === true) {
					this.showSteps = false;
				}
				break;
			case 'snap':
				if (value === true) {
					this.snap = true;
				}
				break;
		}
	}
	testDiv = null;

	// Create component
	this.create();
};

/**
 * Create slider in referenced element
 * by adding child elements and setting event listeners.
 *
 * @return {undefined}
 */
Slider.prototype.create = function()
{
	// Remove child elements
	this.component.innerHTML = '';

	// Add wrapper class
	this.component.className += this.component.className ? ' slider' : 'slider';
	if (this.borderRadiusProperty) {
		this.component.style[this.borderRadiusProperty] = this.radius + "px";
	}

	// Set height/width according to radius
	var diameter = this.radius * 2;
	this.component.style.height = diameter + 'px';
	this.component.style.width = diameter + 'px';

	// Check whether step size is within value range
	var valueRange = (this.max > this.min) ? this.max - this.min : this.min - this.max;
	if (this.step > valueRange) {
		this.step = 1;
		this.showSteps = false;
	}

	// Border background
	if (this.background || !this.showSteps) {
		var border = document.createElement("div");
		border.className = 'slider-border';
		border.style.borderWidth = this.borderSize + 'px';
		if (this.background) {
			border.style.borderColor = this.background;
		}
		if (this.borderRadiusProperty) {
			border.style[this.borderRadiusProperty] = this.radius + "px";
		}
		this.component.appendChild(border);
	}

	/**
	 * Steps
	 */

	// Figure out how many steps the slider will need
	var amountOfSteps = Math.round(valueRange / this.step);

	// Calculate the outline of the slider
	var outline = (this.radius * 2) * Math.PI;
	// Calculate the increment of the angle for each step
	this.stepAngle = 360 / (amountOfSteps + 1);
	// Calculate the width of one step
	var stepWidth = (outline / amountOfSteps);

	// Set width for border step, using 50% of step width as spacing
	var stepSize = stepWidth / 2;
	if (stepSize > 10) {
		stepSize = 10;
	}

	// Calculate start angle for steps
	var center = this.centerCoordinates();
	var startAngle = this.calculateAngle(center, {x: (center.x + (stepSize / 2)), y: (center.y - this.radius)});
	if (this.snap) {
		this.angle = startAngle;
	}

	// Generate border steps
	var currentAngle = startAngle;
	var translateX = (this.radius-(stepSize/2));
	for (var stepCount = amountOfSteps; stepCount >= 0; stepCount--) {
		this.steps.push(currentAngle); 

		if (this.showSteps) {
			// Create border step
			var borderStep = document.createElement("span");
			borderStep.className = 'slider-step';
			borderStep.style.borderWidth = this.borderSize + 'px';
			borderStep.style.width = stepSize + 'px'

			// Rotate border step
			var stepRotate = "translateX(" + translateX + "px) rotate(" + currentAngle + "deg)";
			if (this.transformProperty) {
				borderStep.style[this.transformProperty] = stepRotate;
			}
			// Append border step to slider
			this.component.appendChild(borderStep);
		}

		// Increment angle for next step
		currentAngle += this.stepAngle;
	}

	/**
	 * Overlay
	 */

	// Overlay clipping
	var wrapperRectTop = 0;
	var wrapperRectRight = this.radius * 2;
	var wrapperRectBottom = this.radius * 2;
	var wrapperRectLeft = this.radius;
	var wrapperClip = 'rect('+ wrapperRectTop +'px '+ wrapperRectRight +'px '+ wrapperRectBottom +'px '+ wrapperRectLeft +'px)';

	var overlayLeftRectTop = 0;
	var overlayLeftRectRight = this.radius * 2;
	var overlayLeftRectBottom = this.radius * 2;
	var overlayLeftRectLeft = this.radius;
	var overlayLeftClip = 'rect('+ overlayLeftRectTop +'px '+ overlayLeftRectRight +'px '+ overlayLeftRectBottom +'px '+ overlayLeftRectLeft +'px)';

	var overlayRightRectTop = 0;
	var overlayRightRectRight = this.radius;
	var overlayRightRectBottom = this.radius * 2;
	var overlayRightRectLeft = 0;
	var overlayRightClip = 'rect('+ overlayRightRectTop +'px '+ overlayRightRectRight +'px '+ overlayRightRectBottom +'px '+ overlayRightRectLeft +'px)';

	// Left overlay
	var wrapperLeft = document.createElement("div");
	wrapperLeft.className = 'slider-wrapper-left';
	wrapperLeft.style['clip'] = wrapperClip;
	this.overlayLeft = document.createElement("div");
	this.overlayLeft.className = 'slider-overlay-left';
	this.overlayLeft.style['clip'] = overlayLeftClip;
	this.overlayLeft.style.borderWidth = this.borderSize + 'px';
	if (this.color) {
		this.overlayLeft.style.borderColor = this.color;
	}
	if (this.transformProperty) {
		wrapperLeft.style[this.transformProperty] = "rotate(180deg)";
		this.overlayLeft.style[this.transformProperty] = "rotate(180deg)";
	}
	if (this.borderRadiusProperty) {
		wrapperLeft.style[this.borderRadiusProperty] = this.radius + "px";
		this.overlayLeft.style[this.borderRadiusProperty] = this.radius + "px";
	}
	wrapperLeft.appendChild(this.overlayLeft)
	this.component.appendChild(wrapperLeft);

	// Right overlay
	var wrapperRight = document.createElement("div");
	wrapperRight.className = 'slider-wrapper-right';
	wrapperRight.style['clip'] = wrapperClip;
	this.overlayRight = document.createElement("div");
	this.overlayRight.className = 'slider-overlay-right';
	this.overlayRight.style['clip'] = overlayRightClip;
	this.overlayRight.style.borderWidth = this.borderSize + 'px';
	if (this.color) {
		this.overlayRight.style.borderColor = this.color;
	}
	if (this.borderRadiusProperty) {
		wrapperRight.style[this.borderRadiusProperty] = this.radius + "px";
		this.overlayRight.style[this.borderRadiusProperty] = this.radius + "px";
	}
	wrapperRight.appendChild(this.overlayRight)
	this.component.appendChild(wrapperRight);

	// Button
	this.button = document.createElement("div");
	this.button.className = 'slider-button';
	if (this.borderRadiusProperty) {
		this.button.style[this.borderRadiusProperty] = this.radius + "px";
	}
	this.component.appendChild(this.button);

	// Mapper
	this.mapper = document.createElement("div");
	this.mapper.className = 'slider-mapper';
	if (this.borderRadiusProperty) {
		this.mapper.style[this.borderRadiusProperty] = this.radius + "px";
	}
	this.component.appendChild(this.mapper);


	// Draw slider overlay and set button position
	this.draw();


	// Define events
	var that = this;
	var eventPressed = function(e)
	{
		if (e.target != that.mapper) {
			return;
		}
		// Get offset coordinates for slider
		that.offset = that.getOffset(that.component);
		that.move(e);
		that.sliding = true;
		that.stopEvent(e);
	};
	var eventReleased = function(e)
	{
		that.sliding = false;
		that.stopEvent(e);
	};
	var eventMove = function(e)
	{
		if (!that.sliding) {
			return;
		}

		that.move(e);
		that.stopEvent(e);
	};

	// Set event listeners
	if (window.navigator.msPointerEnabled) {
		document.addEventListener('MSPointerDown', eventPressed, false);
		document.addEventListener('MSPointerMove', eventMove, false);
		document.addEventListener('MSPointerUp', eventReleased, false);
	}
	this.setEvent(document, 'touchstart', eventPressed);
	this.setEvent(document, 'touchmove', eventMove);
	this.setEvent(document, 'touchend', eventReleased);
};

/**
 * Draw button and slider overlay according to current angle position.
 *
 * @return {undefined}
 */
Slider.prototype.draw = function()
{
	// Get current angle position
	var angle = (this.angle % 360);

	// Button position
	if (this.transformProperty) {
		//this.button.style[this.transformProperty] = "rotate(" + angle + "deg)";
		this.button.style[this.transformProperty] = this.convertToRotationMatrix(angle);
	}

	// Slide overlay (Left side)
	var overlayAngleLeft = 180;
	if ( (angle > 180 && angle < 360) || (angle < 0 && angle > -180) ) {
		overlayAngleLeft = angle;
	}
	if (this.transformProperty) {
		//var overlayLeftMatrix = this.convertToRotationMatrix(overlayAngleLeft);
		//this.overlayLeft.style[this.transformProperty] = overlayLeftMatrix;
		this.overlayLeft.style[this.transformProperty] = "rotate(" + overlayAngleLeft + "deg)";
	}

	// Slide overlay (Right side)
	var overlayAngleRight = 0;
	if ( (angle > 0 && angle <= 180) || (angle > -360 && angle <= -180) ) {
		overlayAngleRight = angle;
	} else if (angle > 0 && angle > 180) {
		overlayAngleRight = 180;
	} else if (angle < 0 && angle > -360 && angle > -180) {
		overlayAngleRight = -180;
	}
	if (this.transformProperty) {
		//var overlayRightMatrix = this.convertToRotationMatrix(overlayAngleRight);
		//this.overlayRight.style[this.transformProperty] = overlayRightMatrix;
		this.overlayRight.style[this.transformProperty] = "rotate(" + overlayAngleRight + "deg)";
	}
};

/**
 * Move new angle position on event
 * by user interacting with a pointing device (mouse/finger).
 *
 * @param  {object} e Event containing movement details.
 * @return {undefined}
 */
Slider.prototype.move = function(e)
{
	if (!e) var e = window.event;

	// Get coordinates of last pressed position on screen
	var pressedPosition = this.getPosition(e);

	// Determine coordinates for pressed location inside slider
	var posX = pressedPosition.x - this.offset.x;
	var posY = pressedPosition.y - this.offset.y;

	// Calculate new angle position
	var angle = this.calculateAngle(this.center, {x: posX, y: posY});

	// Snap new angle position
	if (this.snap) {
		var snapAngle = NaN;
		var currentAngle = (angle - (this.stepAngle / 2));
		// Go through steps and find closest step to current angle
		for (var step = this.steps.length - 1; step >= 0; step--) {
			var stepAngle = this.steps[step];
			if (stepAngle <= currentAngle ) {
				snapAngle = stepAngle;
				break;
			}
		}
		// When no closest step found
		if (isNaN(snapAngle)) {
			// Use last entry when close to end
			if (angle <= 360 - (this.stepAngle / 2)) {
				snapAngle = this.steps[this.steps.length-1];
			}
			// Or first entry when near beginning
			else {
				snapAngle = this.steps[0];
			}
		}

		// Set snapped angle position
		angle = snapAngle;
	}

	// Update angle position
	this.angle = angle;

	// Draw slider overlay and set button position
	this.draw();

	// Execute callback function
	if (typeof(this.onMove) == "function") {
		this.onMove();
	}
};

/**
 * Get current value of circle slider.
 *
 * @return {number} Current value of slider.
 */
Slider.prototype.getValue = function()
{
	// Use minimum value as default value
	var value = this.min;

	// Determine value range
	var valueRange = (this.max > this.min) ? this.max - this.min : this.min - this.max;
	// Get current angle position
	var anglePosition = (this.angle + 360) % 360;

	// Calculate value by using snapped step
	if (this.snap) {
		// Get current step number
		var stepNumber = Math.round(anglePosition / this.stepAngle);
		// Add to minimum (default) value
		if (this.max > this.min) {
			value += (this.step * stepNumber);
		}
		// Detract from minimum (default) value
		else {
			value -= (this.step * stepNumber);
		}
	}
	// Calculate value
	else {
		// Calculate the increment of the angle for each value
		var valueAngle = 360 / (valueRange + 1);
		// Get current value number
		var valueNumber = Math.round(anglePosition / valueAngle);

		// Add to minimum (default) value
		if (this.max > this.min) {
			value += (1 * valueNumber);
		}
		// Detract from minimum (default) value
		else {
			value -= (1 * valueNumber);
		}
	}

	// Stay above minimum value
	if ((this.max > this.min && value < this.min) || (this.max < this.min && value > this.min)) {
		value = this.min;
	}

	// Do not exceed maximum value
	if ((this.max > this.min && value > this.max) || (this.max < this.min && value < this.max)) {
		value = this.max;
	}

	// Return value
	return value;
};

/**
 * Calculate radius distance of target point from center.
 *
 * @param  {object} center Center coordinates of circle.
 * @param  {object} point  Target coordinates in circle.
 * @return {number}        Radius distance of target from center.
 */
Slider.prototype.calculateRadius = function(center, point)
{
	return Math.sqrt(
		Math.abs(point.x - center.x) * Math.abs(point.x - center.x) +
		Math.abs(point.y - center.y) * Math.abs(point.y - center.y)
	);
};

/**
 * Calculate angle between two points in circle.
 *
 * @param  {object} center Center coordinates of circle.
 * @param  {object} point  Target coordinates in circle.
 * @return {number}        Angle between two points.
 */
Slider.prototype.calculateAngle = function(center, point)
{
	// Calculate radius distance of target point from center
	var radiusDistance = this.calculateRadius(center, point);
	// Calculate coordinates for minimum value based
	var minimum = {x: center.x, y: center.y - radiusDistance};
	// Calculate angle between two points in circle
	return (2 * Math.atan2(point.y - minimum.y, point.x - minimum.x)) * 180 / Math.PI;
};

/**
 * Convert an angle to coordinates.
 *
 * @param  {object} center Center coordinates of circle.
 * @param  {number} radius Radius distance of target from center.
 * @param  {number} angle  Angle to convert.
 * @return {number}        Coordinates of angle.
 */
Slider.prototype.convertAngleToPoint = function(center, radius, angle)
{
	// Angle in degrees
	angle *= Math.PI / 180;
	return {
		x: center.x + Math.sin(Math.PI - angle) * radius,
		y: center.y + Math.cos(Math.PI - angle) * radius
	};
};

/**
 * Convert an angle to rotation matrix for transform property.
 *
 * @param  {number} angle  Angle to convert.
 * @return {string}        Matrix for transform property.
 */
Slider.prototype.convertToRotationMatrix = function(angle)
{
	var radian = (angle * Math.PI) / 180;
	var sin = Math.sin(radian);
	var cos = Math.cos(radian);

	// Mozilla uses 'px'
	if (this.transformProperty == 'MozTransform') {
		return "matrix(" + cos + ", " + sin + "," + -sin + ", " + cos + ", 0px, 0px)";
	}
	return "matrix(" + cos + ", " + sin + "," + -sin + ", " + cos + ", 0, 0)";
};

/**
 * Find coordinates for circle center of slider.
 *
 * @return {object} Center coordinates of slider.
 */
Slider.prototype.centerCoordinates = function()
{
	// Get circle center of slider
	this.center.x = this.component.clientWidth / 2;
	this.center.y = this.component.clientHeight / 2;

	return this.center;
};

/**
 * Find offset coordinates of an element.
 *
 * @param  {object} element Element to find coordinates.
 * @return {object}         Offset coordinates of element.
 */
Slider.prototype.getOffset = function(element)
{
	var left = 0;
	var top = 0;
	while( element && !isNaN( element.offsetLeft ) && !isNaN( element.offsetTop ) ) {
		left += element.offsetLeft;
		top += element.offsetTop;
		element = element.offsetParent;
	}
	return { y: top, x: left };
};

/**
 * Find coordinates of last touched position on screen,
 * relative to the document.
 *
 * @param  {object} e Event containing movement details.
 * @return {object}   Coordinates of relative to the document.
 */
Slider.prototype.getPosition = function(e)
{
	var left = 0;
	var top = 0;
	if (!e) var e = window.event;
	var touch = e;
	if (e.changedTouches) {
		touch = e.changedTouches[0];
	}
	left = touch.pageX;
	top = touch.pageY;
	// Return coordinates relative to the document
	return { y: top, x: left };
};

/**
 * Get transform property for an element.
 *
 * @param  {object} element Element to search property.
 * @return {string|false}   Transform property name of element.
 */
Slider.prototype.getTransformProperty = function(element)
{
	if (this.transformProperty !== false) {
		return this.transformProperty;
	}

	var properties = [
		'transform',
		'WebkitTransform',
		'msTransform',
		'MozTransform',
		'OTransform'
	];
	var property;
	while (property = properties.shift()) {
		if (typeof element.style[property] != 'undefined') {
			this.transformProperty = property;
			return property;
		}
	}
	return false;
};

/**
 * Get border radius property for an element.
 *
 * @param  {object} element Element to search property.
 * @return {string|false}   Border radius property name of element.
 */
Slider.prototype.getBorderRadiusProperty = function(element)
{
	if (this.borderRadiusProperty !== false) {
		return this.borderRadiusProperty;
	}

	var properties = [
		'borderRadius',
		'WebkitBorderRadius',
		'MozBorderRadius'
	];
	var property;
	while (property = properties.shift()) {
		if (typeof element.style[property] != 'undefined') {
			this.borderRadiusProperty = property;
			return property;
		}
	}
	return false;
};

/**
 * Register an event listener to an element.
 *
 * @param {object}   element   Element that we want to listen for events.
 * @param {string}   eventName A string representing the event type to listen for.
 * @param {Function} listener  The object that receives a notification when an event of the specified type occurs.
 */
Slider.prototype.setEvent = function(element, eventName, listener)
{
	// Internet Explorer
	if (element.attachEvent) {
		element.attachEvent("on" + eventName, listener);
	}
	// Gecko / W3C
	else if (element.addEventListener) {
		element.addEventListener(eventName, listener, false);
	}
	// Other
	else {
		element["on" + eventName] = listener;
	}
};

/**
 * Cancels the event if it is cancelable and
 * prevents further propagation of the current event.
 *
 * @param  {object} event
 * @return {undefined}
 */
Slider.prototype.stopEvent = function(event)
{
	if (!e) var e = window.event;

	// W3C
	if (e.preventDefault) {
		e.preventDefault();
		e.stopPropagation();
	}
	// Internet Explorer
	else {
		e.returnValue=false;
		e.cancelBubble=true;
	}
};